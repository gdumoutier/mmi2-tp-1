#TP MMI2 : mise en application jQuery

### **TP 1** : 

- Suivre les consignes sans éditer l'index.php à l'exception de l'ajout d'appel **css** ou **js**. Aidez-vous du document pdf fourni.
- Inclure la librairie **jQuery** du CDN de votre choix et y appliquer un *Fallback* (ou solution de rechange) vers un **jQuery** local.
- Faire une mise en page CSS basique : header, nav, footer... dans un fichier **style.css** que vous placerez dans un sous-dossier **/css**
- Créer un fichier **app.js** dans le sous dossier **/js**
- Dans ce fichier, instancier **jQuery** dans les meilleurs conditions
- ... puis instancier **[owlCarousel](//owlcarousel2.github.io/OwlCarousel2/)** sur **.slider** de la page **index* (navigation en dots) : Pour cela aider vous de la documentation !
-  Et **[Remodal](https://github.com/vodkabears/Remodal)**. Celui-ci devra presenter un formulaire de contact (non fonctionnel) via l'entré **"contact"** du menu.
-  Enfin, utiliser le plugin **[Autosize](http://www.jacklmoore.com/autosize/)** et l'appliquer au **textarea** de cette modale.
